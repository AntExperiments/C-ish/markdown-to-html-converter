﻿///
/// Program-Name:   Markdown to HTML converter
/// Filename:       Program.cs
/// Description:    Converts a Markdown file to HTML with an optional stylesheet
/// Author:         Yanik Ammann
/// Last Changed:   06.02.2020 by Yanik Ammann
/// Todo:           reference-style links
/// Dependencied:   .NET (mono)
///
/// Versions (version-number, date, author, description):
/// v0.0, 21.01.2020, Yanik Ammann, starting developement
/// v0.1, 21.01.2020, Yanik Ammann, functional prototype
/// v0.2, 21.01.2020, Yanik Ammann, added support bold, italic and italic bold
/// v0.3, 21.01.2020, Yanik Ammann, added support for special characters
/// v0.4, 21.01.2020, Yanik Ammann, added CSS file support
/// v0.5, 21.01.2020, Yanik Ammann, made checkboxes not clickable
/// v0.6, 21.01.2020, Yanik Ammann, optimized code and added more comments
/// v0.7, 22.01.2020, Yanik Ammann, added support for tables
/// v0.8, 22.01.2020, Yanik Ammann, added formating (tabs and line breaks)
/// v0.9, 22.01.2020, Yanik Ammann, compacted CSS to single line (remove Line breaks)
/// v1.0, 22.01.2020, Yanik Ammann, added support for links
/// v1.1, 22.01.2020, Yanik Ammann, added more spaces (aka: optimised code for future expansion)
/// v1.2, 22.01.2020, Yanik Ammann, added counted and uncounted list
/// v1.3, 03.02.2020, Yanik Ammann, added selection (Console.Read()) in order to input filenames
/// v1.4, 03.02.2020, Yanik Ammann, added support for images
/// v1.5, 03.02.2020, Yanik Ammann, fixed <p>-tag placement
/// v1.6, 06.02.2020, Yanik Ammann, issue/1 fixed => Default setting for CSS is NULL now
/// v1.6, 06.02.2020, Yanik Ammann, move everything (every line loop) into a try-catch
/// v1.6, 06.02.2020, Yanik Ammann, issue/2 fixed => Grammar issue
///


using System;
using System.Text.RegularExpressions;

namespace Markdown_to_HTML_converter
{
    class Program
    {
        static void Main(string[] args)
        {
            // runtime variables
            int tableLineBefore = 0;
            bool beenInList = false;
            bool beenInUnsorted = false;
            bool beenInCodeBlock = false;
            bool beenInTaskList = false;

            // setup variables
            string filePath = @"C:\Users\yanik\Downloads\demo.md";
            string exportFilePath = @"C:\Users\yanik\Downloads\demo.html";
            string cssPath = null; /* use NULL for none */

            // input variables
            Console.Write($@"Enter Input-Filepath ({filePath}) ");
            string input1 = Console.ReadLine();
            if (String.IsNullOrEmpty(input1) == false)
                filePath = input1;

            Console.Write($@"Enter Export-Filepath ({exportFilePath}) ");
            string input2 = Console.ReadLine();
            if (String.IsNullOrEmpty(input2) == false)
                exportFilePath = input2;

            Console.Write($@"Enter CSS-Filepath ({cssPath}) (or leave empty for NULL) ");
            string input3 = Console.ReadLine();
            if (String.IsNullOrEmpty(input3) == false)
                cssPath = input3;

            // read file
            string[] text = System.IO.File.ReadAllLines(filePath);

            // delete output file
            System.IO.File.Delete(exportFilePath);

            // insert basic HTML code
            System.IO.File.AppendAllText(exportFilePath, $"<html>\n\t<head>\n\t\t<title>{Regex.Replace(exportFilePath, $@"(.+\\)|\..+", "")}</title>\n\t\t<meta charset=\"utf-8\"/>\n");

            // insert CSS code if needed
            if (!String.IsNullOrEmpty(cssPath))
                System.IO.File.AppendAllText(exportFilePath, $"\t\t<style>\n\t\t\t{System.IO.File.ReadAllText(cssPath).Replace(System.Environment.NewLine, "")}\n\t\t</style>\n");
            System.IO.File.AppendAllText(exportFilePath, "\t</head>\n\t<body>");

            // go through file, line by line
            for (int i = 0; i < text.Length; i++) {
                if (!String.IsNullOrEmpty(text[i])) {
                    try {
                        // code blocks
                        if (text[i] == "```") {
                            if (beenInCodeBlock == false) {
                                beenInCodeBlock = true;
                                text[i] = "<codeblock>";
                            } else {
                                beenInCodeBlock = false;
                                text[i] = "</codeblock><br><br>";
                            }
                        } else if (beenInCodeBlock == false) {
                            // image
                            text[i] = Regex.Replace(text[i], @"!\[(.+)\]\((.+)\)", $"<img src='$2' alt='$1'></img>");

                            // italic and bold
                            text[i] = Regex.Replace(text[i], @"\*\*\*(.*?)\*\*\*", "<strong><i>$1</i></strong>");
                            text[i] = Regex.Replace(text[i], @"___(.*?)___", "<strong><i>$1</i></strong>");

                            // search for break lines
                            if (text[i].Substring(0, 3) == "___" ||
                                text[i].Substring(0, 3) == "---" ||
                                text[i].Substring(0, 3) == "***")
                                text[i] = "<hr>";

                            // bold
                            text[i] = Regex.Replace(text[i], @"\*\*(.*?)\*\*", "<strong>$1</strong>");
                            text[i] = Regex.Replace(text[i], @"__(.*?)__", "<strong>$1</strong>");

                            // italic
                            text[i] = Regex.Replace(text[i], @"\*(.*?)\*", "<i>$1</i>");
                            text[i] = Regex.Replace(text[i], @"_(.*?)_", "<i>$1</i>");

                            // strikethrough
                            text[i] = Regex.Replace(text[i], @"\~\~(.*?)\~\~", "<s>$1</s>");

                            // special characters
                            text[i] = text[i].Replace("(c)", "&copy;").Replace("(C)", "&copy;").Replace("(r)", "&reg;").Replace("(R)", "&reg;").Replace("(tm)", "&trade;").Replace("(TM)", "&trade;").Replace("(p)", "&sect;").Replace("(P)", "&sect;").Replace("+-", "&plusmn;");

                            // links
                            text[i] = Regex.Replace(text[i], @"\[(.+)\]\((.+)\)", $"<a href=\"$2\">$1</a>");

                            // task list
                            if (Regex.IsMatch(text[i], @"- \[ \]") == true || Regex.IsMatch(text[i], @"- \[x\]") == true) {
                                text[i] = Regex.Replace(text[i], @"- \[ \] (.+)", "\t<input type=\"checkbox\" onclick=\"return false\"> $1</input><br>");
                                text[i] = Regex.Replace(text[i], @"- \[x\] (.+)", "\t<input type=\"checkbox\" onclick=\"return false\" checked=\"checked\"> $1</input><br>");
                                if (beenInTaskList == false) {
                                    // not been in task list
                                    // open form tag
                                    text[i] = "<form>\n\t\t" + text[i];
                                    beenInTaskList = true;
                                }
                            } else if (beenInTaskList == true) {
                                // close form tag
                                beenInTaskList = false;
                                text[i - 1] = $"{text[i - 1]}</form>\n";
                            }

                            // tables
                            if (Regex.IsMatch(text[i], @"\| (.+) \| (.+) \|")) {
                                string[] columns = text[i].Split('|');
                                // check if last one was a header
                                if (tableLineBefore == 0) {
                                    string output = "";
                                    // start table
                                    output += "<table>\t\t\t<tr>";
                                    for (int j = 1; j < columns.Length - 1; j++)
                                        output += $"\t\t\t\t<th>{columns[j].Trim()}</th>";
                                    output += "\t\t\t</tr>";
                                    text[i] = output;
                                }
                                // check if last one is not a divider
                                else if (tableLineBefore != 1) {
                                    string output = "";
                                    // start table
                                    output += "\t<tr>";
                                    for (int j = 1; j < columns.Length - 1; j++)
                                        output += $"\t\t\t\t<td>{columns[j].Trim()}</td>";
                                    output += "\t\t\t</tr>";
                                    text[i] = output;
                                } else
                                    text[i] = " ";
                                tableLineBefore++;
                            } else {
                                // check if has to close table
                                if (tableLineBefore != 0)
                                    text[i] = "</table>";

                                // say there is no table anymore (next table will start with a header)
                                tableLineBefore = 0;
                            }

                            // quotes
                            if (text[i][0] == '>')
                                text[i] = $"<blockquote>{text[i].Substring(2, text[i].Length - 2)}</blockquote>";

                            // list (counted)
                            if (Regex.IsMatch(text[i], @"^([0-9]+)\. (.+)") == true) {
                                if (beenInList == true) {
                                    beenInList = true;
                                    text[i] = Regex.Replace(text[i], @"^([0-9]+)\. (.+)", "\t<li value=\"$1\">$2</li>");
                                } else {
                                    beenInList = true;
                                    text[i] = Regex.Replace(text[i], @"^([0-9]+)\. (.+)", "<ol>\n\t\t\t<li value=\"$1\">$2</li>");
                                }
                            } else if (beenInList == true) {
                                beenInList = false;
                                text[i - 1] = $"{text[i - 1]}</ol>\n";
                            }

                            // list (uncounted)
                            if (Regex.IsMatch(text[i], @"^([-, *, +]+) (.+)") == true) {
                                if (beenInUnsorted == true) {
                                    text[i] = Regex.Replace(text[i], @"^([-, *, +]+) (.+)", "\t<li>$2</li>");
                                } else {
                                    beenInUnsorted = true;
                                    text[i] = Regex.Replace(text[i], @"^([-, *, +]+) (.+)", "<ul>\n\t\t\t<li>$2</li>");
                                }
                            } else if (beenInUnsorted == true) {
                                beenInUnsorted = false;
                                text[i - 1] = $"{text[i - 1]}</ul>\n";
                            }

                            // inline code
                            text[i] = Regex.Replace(text[i], @"`([^`]+)`", "<code>$1</code>");

                            // check for headings
                            if (text[i][0] == '#')
                                text[i] = $"<h{text[i].Substring(0, text[i].IndexOf(' ')).Length}>{text[i].Substring(text[i].IndexOf(' ') + 1, text[i].Length - text[i].IndexOf(' ') - 1) }</h{text[i].Substring(0, text[i].IndexOf(' ')).Length}>";
                            // add <p> tags if it's not a heading (or other things that shouldn't have <p>-tags
                            else if (tableLineBefore == 0 && text[i] != "</table>" && text[i] != "</codeblock>" && text[i].Substring(0, 4) != "<img"
                                        && beenInCodeBlock == false && text[i].Substring(0, 4) != "<ol>" && text[i].Substring(0, 4) != "<ul>" && beenInTaskList == false
                                        && text[i].Substring(0, 4) != "<hr>" && text[i].Substring(0, 4) != "<blo" && beenInList == false && beenInUnsorted == false)
                                text[i] = $"<p>{text[i]}</p>";

                            // max 3 exclamation or question marks in a row
                            text[i] = Regex.Replace(text[i], @"!!.+[!]", "!!!");
                            text[i] = Regex.Replace(text[i], @"\?\?.+[\!]", "!!!");
                        }
                    }
                    catch (Exception ex) {
                        Console.WriteLine(ex);
                    }
                }
            }
            // output
            foreach (string item in text)
                System.IO.File.AppendAllText(exportFilePath, $"\t\t{item}\n");
            System.IO.File.AppendAllText(exportFilePath, "\t</body>\n</html>\n");
        }
    }
}



